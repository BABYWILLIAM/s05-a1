<%@ page language="java" 
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	import="java.util.Date"
	import="java.time.ZonedDateTime"
    import="java.time.ZoneId"
    import="java.time.format.DateTimeFormatter"
    import="java.time.format.FormatStyle"
%>
     

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Java Server Pages</title>
</head>
<body>
	<h1>Our Date and Time now is...</h1>
	
	
	
	<% System.out.println("Hello from JSP"); %>
	
	
	<%
		ZonedDateTime dateTime = ZonedDateTime.now();
	%>
	
	
	
	<%!
		ZonedDateTime dateTime = ZonedDateTime.now();
	%>
	

	
	<%
		ZonedDateTime mnla = dateTime.withZoneSameInstant(ZoneId.of("Asia/Manila"));
	%>
	
	
	<%
		ZonedDateTime jpn = dateTime.withZoneSameInstant(ZoneId.of("Asia/Tokyo"));
	%>
	
	
	<%
		ZonedDateTime grmny = dateTime.withZoneSameInstant(ZoneId.of("Europe/Berlin"));
	%>
	

	<%
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	%>
	
	
	<%
		String mnlaTime = mnla.format(formatter);
	%>
	
	
	<%
		String jpnTime = jpn.format(formatter);
	%>
	
	
	<%
		String grmnyTime = grmny.format(formatter);
	%>
	

	<ul>
	<li>Manila: <%= mnlaTime %></li>
	<li>Japan: <%= jpnTime %></li>
	<li>Germany: <%= grmnyTime %></li>
	</ul>


	<%!
		private int initVar = 1;
		private int serviceVar = 1;
		private int destroyVar = 3;
	%>
	


	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init" +initVar);
		
		}
	
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy" +destroyVar);
		}
		
		
	%>
	
	
	<% 
		serviceVar++;
		System.out.println("jspService(): service" +serviceVar);
		String content1 = "content1: " + initVar;
		String content2 = "content2: " + serviceVar;
		String content3 = "content3: " + destroyVar;
	%>
	
	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
	
</body>
</html>